package com.falendysh.teqqedjsontest.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.falendysh.teqqedjsontest.model.Post
import com.falendysh.teqqedjsontest.model.PostWithUserAndComments
import com.falendysh.teqqedjsontest.repository.TeqqedRepoImpl
import com.falendysh.teqqedjsontest.repository.TeqqedRepository
import com.falendysh.teqqedjsontest.repository.db.TeqqedDataBase
import com.falendysh.teqqedjsontest.repository.network.TeqqedRestServiceProvider
import kotlinx.coroutines.launch


class TeqqedViewModel(private val app: Application): AndroidViewModel(app) {

    private var repository: TeqqedRepository = TeqqedRepoImpl(
        TeqqedDataBase.getDataBase(app.applicationContext)?.getDAO()!!,
        TeqqedRestServiceProvider.getTeqqedRestService()
    )

    private val appBusyLiveData = MutableLiveData<Boolean>()
    private val messagesLiveData = MutableLiveData<String>()
    private val postsLiveData = MutableLiveData<List<Post>>()
    private val postDetailsLiveData = MutableLiveData<PostWithUserAndComments>()
    private val selectedPostLiveData = MutableLiveData<Int>()

    fun getAppBusyLiveData(): LiveData<Boolean> {
        return appBusyLiveData
    }

    fun getMessagesLiveData(): LiveData<String> {
        return messagesLiveData
    }

    fun getPostsLiveData(): LiveData<List<Post>> {
        return postsLiveData
    }

    fun getPostDetailsLiveData(): LiveData<PostWithUserAndComments> {
        return postDetailsLiveData
    }

    fun getSelectedPostLiveData(): LiveData<Int> {
        return selectedPostLiveData
    }

    fun selectPost(post: Post) {
        selectedPostLiveData.postValue(post.id)
    }

    fun getPosts() {
        viewModelScope.launch {
            appBusyLiveData.postValue(true)

            val posts = repository.getPosts()
            if (posts.isEmpty()) {
                messagesLiveData.postValue("Posts loading started...")
                repository.downloadPosts().subscribe(
                    {
                        if (it == 200) {
                            viewModelScope.launch {
                                messagesLiveData.postValue("Posts loading completed")
                                postsLiveData.postValue(repository.getPosts())
                                appBusyLiveData.postValue(false)
                            }
                        }
                    },
                    {
                        messagesLiveData.postValue("Sorry, posts loading failed")
                    }
                )
            }
            else {
                postsLiveData.postValue(posts)
                appBusyLiveData.postValue(false)
            }

            val users = repository.getUsers()
            if (users.isEmpty()) {
                messagesLiveData.postValue("Users loading started...")
                appBusyLiveData.postValue(true)
                repository.downloadUsers().subscribe(
                    {
                        if (it == 200) {
                            viewModelScope.launch {
                                messagesLiveData.postValue("Users loading completed")
                                appBusyLiveData.postValue(false)
                            }
                        }
                    },
                    {
                        messagesLiveData.postValue("Sorry, users loading failed")
                    }
                )
            }
            else {
                appBusyLiveData.postValue(false)
            }

            val comments = repository.getComments()
            if (comments.isEmpty()) {
                messagesLiveData.postValue("Comments loading started...")
                appBusyLiveData.postValue(true)
                repository.downloadComments().subscribe(
                    {
                        if (it == 200) {
                            viewModelScope.launch {
                                messagesLiveData.postValue("Comments loading completed")
                                appBusyLiveData.postValue(false)
                            }
                        }
                    },
                    {
                        messagesLiveData.postValue("Sorry, comments loading failed")
                    }
                )
            }
            else {
                appBusyLiveData.postValue(false)
            }
        }
    }

    fun getPostDetails(postId: Int) {
        viewModelScope.launch {
            appBusyLiveData.postValue(true)
            postDetailsLiveData.postValue(repository.getPostDetails(postId))
            appBusyLiveData.postValue(false)
        }
    }

}