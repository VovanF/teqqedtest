package com.falendysh.teqqedjsontest.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Comment(
    @SerializedName("postId")
    @ColumnInfo(name = "POST_ID")
    val postId: Int,

    @SerializedName("id")
    @ColumnInfo(name = "ID")
    @PrimaryKey(autoGenerate = false)
    val id: Int,

    @SerializedName("name")
    @ColumnInfo(name = "COMMENT_NAME")
    val name: String,

    @SerializedName("email")
    @ColumnInfo(name = "E_MAIL")
    val email: String,

    @SerializedName("body")
    @ColumnInfo(name = "BODY")
    val body: String
)