package com.falendysh.teqqedjsontest.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Post(
    @SerializedName("userId")
    @ColumnInfo(name = "USER_ID")
    val userId: Int,

    @SerializedName("id")
    @ColumnInfo(name = "ID")
    @PrimaryKey(autoGenerate = false)
    val id: Int,

    @SerializedName("title")
    @ColumnInfo(name = "TITLE")
    val title: String,

    @SerializedName("body")
    @ColumnInfo(name = "BODY")
    val body: String
)