package com.falendysh.teqqedjsontest.model

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

data class Company(
    @SerializedName("name")
    @ColumnInfo(name = "COMPANY_NAME")
    val name: String,

    @SerializedName("catchPhrase")
    @ColumnInfo(name = "CATCH_PHRASE")
    val catchPhrase: String,

    @SerializedName("bs")
    @ColumnInfo(name = "BS")
    val bs: String
)