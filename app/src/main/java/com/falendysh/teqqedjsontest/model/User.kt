package com.falendysh.teqqedjsontest.model

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class User(
    @SerializedName("id")
    @ColumnInfo(name = "ID")
    @PrimaryKey(autoGenerate = false)
    val id: Int,

    @SerializedName("name")
    @ColumnInfo(name = "NAME")
    val name: String,

    @SerializedName("username")
    @ColumnInfo(name = "USER_NAME")
    val userName: String,

    @SerializedName("email")
    @ColumnInfo(name = "E_MAIL")
    val email: String,

    @SerializedName("address")
    @Embedded
    val address: Address,

    @SerializedName("phone")
    @ColumnInfo(name = "PHONE")
    val phone: String,

    @SerializedName("website")
    @ColumnInfo(name = "WEB_SITE")
    val website: String,

    @SerializedName("company")
    @Embedded
    val company: Company
)