package com.falendysh.teqqedjsontest.model

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

data class Geo(
    @SerializedName("lat")
    @ColumnInfo(name = "LAT")
    val lat: Float,

    @SerializedName("lng")
    @ColumnInfo(name = "LNG")
    val lng: Float
)