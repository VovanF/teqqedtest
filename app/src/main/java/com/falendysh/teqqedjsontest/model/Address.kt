package com.falendysh.teqqedjsontest.model

import androidx.room.ColumnInfo
import androidx.room.Embedded
import com.google.gson.annotations.SerializedName

data class Address(
    @SerializedName("street")
    @ColumnInfo(name = "STREET")
    val street: String,

    @SerializedName("suite")
    @ColumnInfo(name = "SUITE")
    val suite: String,

    @SerializedName("city")
    @ColumnInfo(name = "CITY")
    val city: String,

    @SerializedName("zipcode")
    @ColumnInfo(name = "ZIP_CODE")
    val zipCode: String,

    @SerializedName("geo")
    @Embedded
    val geo: Geo
)