package com.falendysh.teqqedjsontest.model

import androidx.room.Embedded
import androidx.room.Relation

data class PostWithUserAndComments(
    @Embedded
    val post: Post,

    @Relation(
        parentColumn = "USER_ID",
        entityColumn = "ID"
    )
    val user: User,

    @Relation(
        parentColumn = "ID",
        entityColumn = "POST_ID"
    )
    val comments: List<Comment>
)