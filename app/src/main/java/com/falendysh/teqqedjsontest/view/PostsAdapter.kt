package com.falendysh.teqqedjsontest.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.falendysh.teqqedjsontest.R
import com.falendysh.teqqedjsontest.model.Post
import kotlinx.android.synthetic.main.post_list_item_view.view.*

class PostsAdapter(private val context: Context): RecyclerView.Adapter<PostsAdapter.ViewHolder>() {

    private val data = ArrayList<Post>()

    var selectionListener: PostSelectionListener? = null

    fun setData(posts: List<Post>) {
        data.clear()
        data.addAll(posts)
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val titleTextView: TextView = itemView.postTitleTextView
        val bodyTextView: TextView = itemView.postBodyTextView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.post_list_item_view, parent, false))
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.titleTextView.text = data[position].title
        holder.bodyTextView.text = data[position].body
        holder.itemView.setOnClickListener {
            selectionListener?.onPostSelected(data[position])
        }
    }

    interface PostSelectionListener{
        fun onPostSelected(post: Post)
    }
}