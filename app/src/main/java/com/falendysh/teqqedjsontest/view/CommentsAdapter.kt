package com.falendysh.teqqedjsontest.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.falendysh.teqqedjsontest.R
import com.falendysh.teqqedjsontest.model.Comment
import kotlinx.android.synthetic.main.comment_list_item_layout.view.*

class CommentsAdapter (private val context: Context): RecyclerView.Adapter<CommentsAdapter.ViewHolder>() {

    private val data = ArrayList<Comment>()

    fun setData(comments: List<Comment>) {
        data.clear()
        data.addAll(comments)
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nameTextView: TextView = itemView.commentNameTextView
        val emailTextView: TextView = itemView.commentEmailTextView
        val bodyTextView: TextView = itemView.commentBodyTextView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.comment_list_item_layout, parent, false))
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.nameTextView.text = data[position].name
        holder.emailTextView.text = data[position].email
        holder.bodyTextView.text = data[position].body
    }
}