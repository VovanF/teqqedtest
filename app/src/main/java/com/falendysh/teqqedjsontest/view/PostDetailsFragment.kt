package com.falendysh.teqqedjsontest.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.observe
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.falendysh.teqqedjsontest.R
import com.falendysh.teqqedjsontest.model.User
import com.falendysh.teqqedjsontest.viewmodel.TeqqedViewModel
import kotlinx.android.synthetic.main.post_detail_fragment_layout.view.*

class PostDetailsFragment: Fragment() {

    private val viewModel: TeqqedViewModel by activityViewModels()
    private lateinit var adapter: CommentsAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.post_detail_fragment_layout, container, false).apply {

            adapter = CommentsAdapter(requireContext())

            this.commentsRecyclerView.adapter = adapter
            this.commentsRecyclerView.layoutManager = LinearLayoutManager(requireContext())

            viewModel.getPostDetailsLiveData().observe(viewLifecycleOwner) {
                this.postDetailsTitleTextView.text = it.post.title
                this.userInfoTextView.text = displayUser(it.user)
                this.postDetailsBodyTextView.text = it.post.body
                adapter.setData(it.comments)
            }

            viewModel.getSelectedPostLiveData().observe(viewLifecycleOwner) {
                viewModel.getPostDetails(it)
            }
        }
    }


    private fun displayUser(user: User):String {
        val stringBuilder = StringBuilder()
        stringBuilder.append("User information: \n")
        stringBuilder.append("${user.name} (${user.userName}) \n")
        stringBuilder.append("E-mail: ${user.email} \n")
        stringBuilder.append("Address: \n")
        stringBuilder.append("${user.address.suite}, ${user.address.street}, ${user.address.city}, ${user.address.zipCode} \n")
        stringBuilder.append("${user.address.geo} \n")
        stringBuilder.append("Phone: ${user.phone} \n")
        stringBuilder.append("WebSite: ${user.website} \n")
        stringBuilder.append("Company: ${user.company.name} (${user.company.catchPhrase}) \n")
        stringBuilder.append(user.company.bs)
        return stringBuilder.toString()
    }

}