package com.falendysh.teqqedjsontest.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.observe
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.falendysh.teqqedjsontest.R
import com.falendysh.teqqedjsontest.model.Post
import com.falendysh.teqqedjsontest.viewmodel.TeqqedViewModel
import kotlinx.android.synthetic.main.post_list_fragment_layout.view.*

class PostListFragment: Fragment(), PostsAdapter.PostSelectionListener {

    private val viewModel: TeqqedViewModel by activityViewModels()

    var adapter: PostsAdapter? = null

    override fun onStart() {
        super.onStart()
        viewModel.getPosts()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        adapter = PostsAdapter(requireContext())
        adapter?.selectionListener = this

        return inflater.inflate(R.layout.post_list_fragment_layout, container, false).apply {
            this.postsRecyclerView.adapter = adapter
            this.postsRecyclerView.layoutManager = LinearLayoutManager(context)

            viewModel.getPostsLiveData().observe(viewLifecycleOwner) {
                adapter?.setData(it)
            }
        }
    }

    override fun onPostSelected(post: Post) {
        viewModel.selectPost(post)
    }

}