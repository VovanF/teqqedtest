package com.falendysh.teqqedjsontest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.observe
import com.falendysh.teqqedjsontest.view.PostDetailsFragment
import com.falendysh.teqqedjsontest.view.PostListFragment
import com.falendysh.teqqedjsontest.viewmodel.TeqqedViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val viewModel by viewModels<TeqqedViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel.getAppBusyLiveData().observe(this) {
            busySpinner.visibility = if (it) View.VISIBLE else View.INVISIBLE
        }

        viewModel.getMessagesLiveData().observe(this) {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        }

        viewModel.getSelectedPostLiveData().observe(this) {
            navigateToPostDetailsScreen()
        }

        navigateToPostListScreen()
    }

    private fun navigateToPostListScreen(){
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragmentContainer, PostListFragment())
        transaction.commitAllowingStateLoss()
    }

    private fun navigateToPostDetailsScreen() {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragmentContainer, PostDetailsFragment())
        transaction.addToBackStack(null)
        transaction.commitAllowingStateLoss()
    }
}
