package com.falendysh.teqqedjsontest.repository

import com.falendysh.teqqedjsontest.model.Comment
import com.falendysh.teqqedjsontest.model.Post
import com.falendysh.teqqedjsontest.model.PostWithUserAndComments
import com.falendysh.teqqedjsontest.model.User
import io.reactivex.rxjava3.subjects.ReplaySubject

interface TeqqedRepository {
    suspend fun getPosts(): List<Post>
    suspend fun getUsers(): List<User>
    suspend fun getComments(): List<Comment>
    suspend fun getPostDetails(postId: Int): PostWithUserAndComments
    fun downloadPosts(): ReplaySubject<Int>
    fun downloadUsers(): ReplaySubject<Int>
    fun downloadComments(): ReplaySubject<Int>
}