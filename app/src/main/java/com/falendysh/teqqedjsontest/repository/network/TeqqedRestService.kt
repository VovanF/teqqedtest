package com.falendysh.teqqedjsontest.repository.network

import com.falendysh.teqqedjsontest.model.Comment
import com.falendysh.teqqedjsontest.model.Post
import com.falendysh.teqqedjsontest.model.User
import retrofit2.Call
import retrofit2.http.GET

interface TeqqedRestService {

    @GET("posts")
    fun loadPosts(): Call<List<Post>>

    @GET("comments")
    fun loadComments(): Call<List<Comment>>

    @GET("users")
    fun loadUsers(): Call<List<User>>

}