package com.falendysh.teqqedjsontest.repository.db

import androidx.room.*
import com.falendysh.teqqedjsontest.model.Comment
import com.falendysh.teqqedjsontest.model.Post
import com.falendysh.teqqedjsontest.model.PostWithUserAndComments
import com.falendysh.teqqedjsontest.model.User

@Dao
interface TeqqedDAO {

    @Query("SELECT * FROM Post")
    fun getPosts(): List<Post>

    @Query("SELECT * FROM Comment")
    fun getComments(): List<Comment>

    @Query("SELECT * FROM User")
    fun getUsers(): List<User>

    @Transaction
    @Query("SELECT * FROM Post WHERE ID == :postId")
    fun getPostWithUserAndComments(postId: Int): PostWithUserAndComments

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPosts(posts: List<Post>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUsers(users: List<User>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertComments(comments: List<Comment>)

}