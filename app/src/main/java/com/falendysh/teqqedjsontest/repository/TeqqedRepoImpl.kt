package com.falendysh.teqqedjsontest.repository

import com.falendysh.teqqedjsontest.model.Comment
import com.falendysh.teqqedjsontest.model.Post
import com.falendysh.teqqedjsontest.model.PostWithUserAndComments
import com.falendysh.teqqedjsontest.model.User
import com.falendysh.teqqedjsontest.repository.db.TeqqedDAO
import com.falendysh.teqqedjsontest.repository.network.TeqqedRestService
import io.reactivex.rxjava3.subjects.ReplaySubject
import kotlinx.coroutines.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception
import java.util.concurrent.atomic.AtomicInteger

class TeqqedRepoImpl(val dao: TeqqedDAO, val restService: TeqqedRestService): TeqqedRepository {

    override suspend fun getPosts(): List<Post> = withContext(Dispatchers.IO) {
        return@withContext dao.getPosts()
    }

    override suspend fun getUsers(): List<User> = withContext(Dispatchers.IO) {
        return@withContext dao.getUsers()
    }

    override suspend fun getComments(): List<Comment> = withContext(Dispatchers.IO) {
        return@withContext dao.getComments()
    }

    override suspend fun getPostDetails(postId: Int): PostWithUserAndComments = withContext(Dispatchers.IO){
        return@withContext dao.getPostWithUserAndComments(postId)
    }

    override fun downloadPosts(): ReplaySubject<Int> {
        val replaySubject = ReplaySubject.create<Int>()
        restService.loadPosts().enqueue(object: Callback<List<Post>> {
            override fun onFailure(call: Call<List<Post>>, t: Throwable) {
                replaySubject.onError(t)
            }

            override fun onResponse(call: Call<List<Post>>, response: Response<List<Post>>) {
                val posts = response.body();
                if (response.isSuccessful && posts != null) {
                    CoroutineScope(Dispatchers.IO).launch {
                        dao.insertPosts(posts)
                        withContext(Dispatchers.Main) {replaySubject.onNext(SUCCESS)}
                    }
                }
                else {
                    replaySubject.onError(EMPTY_RESPONSE_EXCEPTION)
                }
            }
        })

        return replaySubject
    }

    override fun downloadUsers(): ReplaySubject<Int> {
        val replaySubject = ReplaySubject.create<Int>()
        restService.loadUsers().enqueue(object: Callback<List<User>> {
            override fun onFailure(call: Call<List<User>>, t: Throwable) {
                CoroutineScope(Dispatchers.Main).launch {
                    replaySubject.onError(t)
                }
            }

            override fun onResponse(call: Call<List<User>>, response: Response<List<User>>) {
                val users = response.body();
                if (response.isSuccessful && users != null) {
                    CoroutineScope(Dispatchers.IO).launch {
                        dao.insertUsers(users)
                        withContext(Dispatchers.Main) {replaySubject.onNext(SUCCESS)}
                    }
                }
                else {
                    replaySubject.onError(EMPTY_RESPONSE_EXCEPTION)
                }
            }
        })
        return replaySubject
    }

    override fun downloadComments(): ReplaySubject<Int> {
        val replaySubject = ReplaySubject.create<Int>()
        restService.loadComments().enqueue(object: Callback<List<Comment>> {
            override fun onFailure(call: Call<List<Comment>>, t: Throwable) {
                replaySubject.onError(t)
            }

            override fun onResponse(call: Call<List<Comment>>, response: Response<List<Comment>>) {
                val comments = response.body();
                if (response.isSuccessful && comments != null) {
                    CoroutineScope(Dispatchers.IO).launch {
                        dao.insertComments(comments)
                        withContext(Dispatchers.Main) {replaySubject.onNext(SUCCESS)}
                    }
                }
                else {
                    replaySubject.onError(EMPTY_RESPONSE_EXCEPTION)
                }
            }
        })
        return replaySubject
    }

    companion object {
        val EMPTY_RESPONSE_EXCEPTION = Exception("Empty Response")
        const val SUCCESS = 200
    }
}