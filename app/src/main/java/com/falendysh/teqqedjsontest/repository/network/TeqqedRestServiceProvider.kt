package com.falendysh.teqqedjsontest.repository.network

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class TeqqedRestServiceProvider {

    companion object {

        private const val BASE_URL = "http://jsonplaceholder.typicode.com/"

        private fun buildRetrofit(): Retrofit {
            val gson = GsonBuilder()
                .setLenient()
                .create()

            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build()

            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
        }

        private val retrofit = buildRetrofit()

        public fun getTeqqedRestService(): TeqqedRestService {
            return retrofit.create(TeqqedRestService::class.java)
        }

    }
}