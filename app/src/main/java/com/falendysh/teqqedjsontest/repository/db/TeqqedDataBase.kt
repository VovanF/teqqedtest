package com.falendysh.teqqedjsontest.repository.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.falendysh.teqqedjsontest.model.Comment
import com.falendysh.teqqedjsontest.model.Post
import com.falendysh.teqqedjsontest.model.User


@Database(entities = [
    User::class,
    Post::class,
    Comment::class],
    version = 1)

abstract class TeqqedDataBase: RoomDatabase() {

    abstract fun getDAO(): TeqqedDAO

    companion object {
        var INSTANCE: TeqqedDataBase? = null

        const val DATABASE_NAME = "TeqqedDB"

        fun getDataBase(context: Context): TeqqedDataBase? {
            if (INSTANCE == null) {
                synchronized(TeqqedDataBase::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext, TeqqedDataBase::class.java, DATABASE_NAME).build()
                }
            }
            return INSTANCE
        }
    }
}